FROM debian:buster
LABEL maintainer="Hans Driller <hans.driller@gmail.com>"


ENV DEBIAN_FRONTEND noninteractive

# Install required packages for repository and install backy2
RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y \
        arping \
        arptables \
        curl \
        dnsutils \
        iperf \
        iperf3 \
        iproute2 \
        iputils-ping \
        jq \
        ldap-utils \
        less \
        mtr \
        net-tools \
        netcat \
        netcat-openbsd \
        openssl \
        tcpdump \
        telnet \
        traceroute \
        tcptraceroute \
        ngrep \
        vim \
        wget && \
    apt-get clean all && \
    mv /usr/sbin/tcpdump /usr/bin/tcpdump && \
    mv /usr/sbin/traceroute /usr/bin/traceroute

WORKDIR /root
